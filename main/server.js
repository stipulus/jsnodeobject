var express = require('express');
var models = require('./www/Models');

var data,callback,params,CONN;

var app = express();
var http = require('http');
var server = http.Server(app);

app.use('/',express.static(__dirname + '/www'));

server.listen(3000, function () {
  var host = server.address().address;
  var port = server.address().port;

  console.log('JSNodeObject listening at http://%s:%s', host, port);

});

function init(req,res,cb) {
    data = {};
    var start = req.url.indexOf('?');
    if(start >= 0) {
        //console.log(decodeURIComponent(req.url.substr(start+1)))
        params = JSON.parse(decodeURIComponent(req.url.substr(start+1)));
    } else {
        params = {};
    }
    if(cb) {
        cb();
    }
}

function output(res) {
    res.send(JSON.stringify(data));
}

app.get('/model/list', function (req,res) {
    init(req, res, function () {
        console.log('list '+params.model);
        var where = {
            //status: 'submitted'
        };
        models[params.model].prototype.list(params.where,req, function (success) {
            data.ack = (success)?'success':'failure';
            if(success) data.list = success;
            output(res);
        });
    });
});

app.put('/model', function (req, res) {
    init(req, res, function () {
        console.log('put '+params.model+' '+params.id);
        var model = new models[params.model](params.data);
        model.save(function (success) {
            data.ack = (success)?'success':'failure';
            output(res);
        });
    });
});

app.get('/model', function (req, res) {
    init(req, res, function () {
        console.log('get '+params.model+' '+JSON.stringify(params));
        var model = new models[params.model]({});
        model.id = params.id;
        model.get(req,function (success) {
            data.ack = (success)?'success':'failure';
            if(success) data.model = success;
            output(res);
        });
    });
    
});

app.delete('/model', function (req, res) {
    init(req, res, function () {
        console.log('delete '+params.model+' '+JSON.stringify(params));
        var model = new models[params.model]({});
        model.id = params.id;
        model.delete(req,function (success) {
            data.ack = (success)?'success':'failure';
            output(res);
        });
    });
});







