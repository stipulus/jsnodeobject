var isNode = (typeof document === 'undefined');
var DB_EXISTS = false;
var CONN = null;
var PURGE = true;
var socket = null;
var PURGE = false;

var GET = 0;
var PUT = 1;
var DELETE = 2;

if(isNode) {
    var oopi = require('./libs/oopi/oopi');
    var http = require('http');
    var mysql = require('mysql');
    var CONFIG = require('../config.js');
    var JSNodeObject = require('./libs/JSNodeObject/JSNodeObject');
    JSNodeObject.configure({
        HOST:CONFIG.HOST,
        USER:CONFIG.USER,
        PASS:CONFIG.PASS,
        PORT:CONFIG.PORT,
        DB_NAME:CONFIG.DB_NAME
    });
} else {
    __dirname = "";
}

var Item = JSNodeObject.extend({
    dbparams: {
        name:'varchar(100)',
        qty:'int'
    },
    className: 'Item',
    construct: function () {

    }
});

var safeTimeout = {
    running: false,
    interval: 100,
    syncInterval: 3000,
    current:0,
    last:0,
    startTime:0,
    waiting:{},
    start: function () {
        this.running = true;
        this.startTime = (new Date()).getTime();
        this.startTime -= this.startTime % this.interval;
        this.current = this.startTime;
        this.run();
    },
    run: function () {
        //console.log(this.running);
        if(this.running) {
            var diff = 0;
            if(this.current % this.syncInterval === 0) {
                var current = (new Date()).getTime();
                if(current >= this.last) this.stop();
                while(this.current < current-this.interval)
                    this.current += this.interval;
                diff = current-this.current;
                //console.log(this.current,current,diff,this.interval-diff);
            }
            //console.log(this.current-(new Date()).getTime(),diff);
            setTimeout(function () {
                safeTimeout.run()
            },this.interval-diff);
            this.current += this.interval;
            this.call();
        }
    },
    call: function () {
        if(this.waiting[this.current])
            while(this.waiting[this.current].length > 0)
                this.waiting[this.current].pop()();
    },
    stop: function () {
        this.running = false;
    },
    set: function (fun,time) {
        time = time || 0;
        if(typeof time === 'function' && typeof fun === 'number') {
            var temp = fun;
            fun = time;
            time = temp;
        }
        time = (new Date()).getTime()+time;
        time -= time % this.interval;

        if(time > this.last)
            this.last = time;

        if(!this.waiting[time])
            this.waiting[time] = new Array();
        this.waiting[time].push(fun);

        if(!this.running) this.start();

        return {time:time,i:this.waiting[time].length-1};
    },
    remove: function (hash) {
        if(this.waiting[hash.time])
            this.waiting[hash.time].splice(hash.i,1);
    }
};

if(isNode) {

    if(PURGE) {
        query('drop database '+CONFIG.DB_NAME, function () {
            
        });
        DB_EXISTS = false;
        console.log('purge');
        //setup();
    } //else setup();
    exports.Item = Item;
}