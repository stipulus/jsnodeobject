var isNode = (typeof document === 'undefined');
//Seperate library for making objects
if(typeof oopi === 'undefined' || isNode) {
    var oopi = {};
    (function () {
        var pub = oopi;
        var rand = {
            len: 6,
            chars: 'abacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',
            gen: function () {
                var str = '';
                var charlen = this.chars.length;
                for(var i = this.len;i > 0;i--)
                    str += this.chars.charAt(Math.floor(Math.random()*charlen));
                return str;
            }
        };
        function newobj(obj) {
            if(obj === 'object') {
                var newobj = {};
                for(var i in obj)
                    newobj[i] = newobj(obj[i]);
                return newobj;
            } else {
                return obj;
            }
        }
        var base = {
            extend: function (child,abstract) {
                child.__constructors = new Array();
                if(this.__constructors) 
                    child.__constructors = child.__constructors.concat(this.__constructors);
                    
                if(typeof child.construct === 'function') {
                    child.__constructors.unshift(child.construct);
                }
                for(var i in this)
                    if(i === '__constructors');//do nothing
                    else if(typeof child[i] === 'undefined' && i !== 'super') {
                        child[i] = newobj(this[i]);
                    } else {
                        if(typeof child.super === 'undefined') {
                            child.super = {};
                        }
                        if(typeof this[i] === 'function') {
                            if(i !== '__constructors')
                                child.super[i] = this[i].bind(child);
                        } else {
                            child.super[i] = newobj(this[i]);
                        }
                    }
                    
                function F() {
                    if(abstract)
                        throw new Error('Cannot construct abstract class.');
                    for(var i = child.__constructors.length-1;i >= 0;i--)
                        child.__constructors[i].apply(this,arguments);
                }
                F.prototype = child;
                return F;
            }
        };
        pub.abstract = function (child) {
            return base.extend(child,true);
        };
        pub.class = function (child) {
            return base.extend(child);
        };
    })();
}
var JSNodeObject = (function () {
    var CONN,CONFIG,DB_EXISTS;
    var CONN_TIMEOUT = 2000;
    if(isNode) {
        var mysql = require('mysql');
    }

    function configure(options) {
        CONFIG = options;
    }

    function openMysql(cb) {
        if(!CONN) {
            CONN = true;
            CONN = mysql.createConnection({
                host: CONFIG.HOST,
                port: CONFIG.PORT,
                user: CONFIG.USER,
                password: CONFIG.PASS
            });
            CONN.config.queryFormat = function (query, values) {
                if (!values)
                    return query;
                return query.replace(/\:(\w+)/g, function (txt, key) {
                    if (values.hasOwnProperty(key)) {
                        return this.escape(values[key]);
                    }
                    return txt;
                }.bind(this));
            };
            CONN.connect(function (err) {
                if(err)
                    console.log(err);
            });
            setup(cb);
            query('use '+CONFIG.DB_NAME);
            safeTimeout.set(function () {
                closeMysql();
            },CONN_TIMEOUT);
        }
        return CONN;
    }

    function query (q,data,cb) {
        var obj = {};
        try {
            obj.q = conn().query(q,data,cb);
        } catch (e) {
            closeMysql();
            openMysql(function () {
                obj.q = conn().query(q,data,cb);
            });
        }
        return obj.q;
    }

    function conn () {
        return openMysql();
    }

    function closeMysql() {
        if(CONN)
            CONN.end();
        CONN = null;
    }

    function dbexists(iftrue,iffalse) {
        query('SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA where SCHEMA_NAME = "?";',[CONFIG.DB_NAME], function (err,rows,fields) {
            if(!rows || rows.length === 0) {
                if(typeof iffalse === 'function') iffalse(rows);
            } else if(rows.length > 0) {
                if(typeof iftrue === 'function') iftrue();
            }
            if(err) {
                console.log(err);
                return;
            }
        });
    }

    function setup(cb) {
        if(!DB_EXISTS) {
            console.log('create db if not exists');
            var q = query('create database if not exists '+CONFIG.DB_NAME, function (err,res) {
                if(err)
                    return console.log(err,q.sql);
            });
            DB_EXISTS = true;
        }
        if(cb)cb();
    }

    var services = {
        get: function (options) {
            //options.data.method = GET;
            options.method = 'GET';
            return this.ajax(options);
        },
        put: function (options) {
            //options.data.method = PUT;
            options.method = 'PUT';
            return this.ajax(options);
        },
        delete: function (options) {
            //options.data.method = DELETE;
            options.method = 'DELETE';
            return this.ajax(options);
        },
        ajax: function (options) {
            //if(!sessionid) sessionid = (new Cookie('session')).get();
            options = options || {};
            options.data = options.data || {};
            //if(typeof options.data.session !== 'undefined') throw new Error('Cannot use key name session.');
            //options.data.session = sessionid;
            //console.log(options);
            return $.ajax({
                method:options.method,
                url:(options.path || options.url || '')+'?'+encodeURIComponent(JSON.stringify(options.data)),
                //url:document.location.protocol+'//'+document.location.hostname+':3000'+(options.path || options.url || '')+'?'+encodeURIComponent(JSON.stringify(options.data)),
                contentType:'text/html',
                error: function (xhr) {
                    //alert("Error sending request.");
                    console.log(xhr);
                    setTimeout(function () {
                        console.log(xhr);
                    },5000);
                }
            });
        }
    };
    var rand = {
        len: 100,
        idLen:7,
        chars: 'abacdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890',
        gen: function (len) {
            len = len || this.len;
            var str = this.chars.charAt(Math.floor(Math.random()*charlen-10));
            var charlen = this.chars.length;
            for(var i = len;i > 1;i--)
                str += this.chars.charAt(Math.floor(Math.random()*charlen));
            return str;
        },
        getId: function () {
            var str = '';
            var charlen = this.chars.indexOf('1')-1;
            for(var i = this.idLen;i > 0;i--)
                str += this.chars.charAt(Math.floor(Math.random()*charlen));
            if($('#'+str).length > 0)
                return this.getId();
            else
                return str;
        }
    };
    var Hurdle = oopi.class({
        i:null,
        callback: null,
        construct: function (callback) {
            this.i = 1;
            if(typeof callback === 'function') this.callback = callback;
        },
        set: function () {
            this.i++;
        },
        complete: function (options) {
            this.i--;
            if(this.i <= 0 && typeof this.callback === 'function') {
                this.callback(options);
                this.i = 0;
            }
        }
    });
    var safeTimeout = {
        running: false,
        interval: 100,
        syncInterval: 3000,
        current:0,
        last:0,
        startTime:0,
        waiting:{},
        start: function () {
            this.running = true;
            this.startTime = (new Date()).getTime();
            this.startTime -= this.startTime % this.interval;
            this.current = this.startTime;
            this.run();
        },
        run: function () {
            //console.log(this.running);
            if(this.running) {
                var diff = 0;
                if(this.current % this.syncInterval === 0) {
                    var current = (new Date()).getTime();
                    if(current >= this.last) this.stop();
                    while(this.current < current-this.interval)
                        this.current += this.interval;
                    diff = current-this.current;
                    //console.log(this.current,current,diff,this.interval-diff);
                }
                //console.log(this.current-(new Date()).getTime(),diff);
                setTimeout(function () {
                    safeTimeout.run()
                },this.interval-diff);
                this.current += this.interval;
                this.call();
            }
        },
        call: function () {
            if(this.waiting[this.current])
                while(this.waiting[this.current].length > 0)
                    this.waiting[this.current].pop()();
        },
        stop: function () {
            this.running = false;
        },
        set: function (fun,time) {
            time = time || 0;
            if(typeof time === 'function' && typeof fun === 'number') {
                var temp = fun;
                fun = time;
                time = temp;
            }
            time = (new Date()).getTime()+time;
            time -= time % this.interval;

            if(time > this.last)
                this.last = time;

            if(!this.waiting[time])
                this.waiting[time] = new Array();
            this.waiting[time].push(fun);

            if(!this.running) this.start();

            return {time:time,i:this.waiting[time].length-1};
        },
        remove: function (hash) {
            if(this.waiting[hash.time])
                this.waiting[hash.time].splice(hash.i,1);
        }
    };
    var SQLObject = oopi.class({
        default_dbparams: {
            id:'varchar(101)',
            ts:'varchar(255)'
        },
        dbparams: {
            name:'varchar(100)'
        },
        className: 'SQLObject',
        id:'',
        ts:0,
        construct: function (obj,cb) {
            switch(typeof obj) {
            case 'object':
                this.loadObj(obj);
                break;
            case 'string':
                this.id = obj;
                this.get(obj,cb);
                break;
            case 'undefined':
            case 'null':
                this.ts = (new Date()).toUTCString();
                this.id = rand.gen();
                break;
            case 'boolean':
                if(obj) {

                } else {

                }
                break;
            }
        },
        loadObj: function (obj) {
            if(!obj) return;
            for(var i in this.default_dbparams)
                if(typeof obj[i] !== 'undefined')
                    this[i] = obj[i];
            for(var i in this.dbparams)
                if(typeof obj[i])
                    this[i] = obj[i];
        },
        query: function (str,cb) {
            query(str,this.getDBParams(),function (err,res) {
                if(err) {
                    console.log(err);
                    return;
                }
                if(cb) cb(res);
            });
        },
        getDBParams: function () {
            var obj = {};
            for(var i in this.default_dbparams) {
                obj[i] = this[i];
            }
            for(var i in this.dbparams) {
                obj[i] = this[i];
            }
            return obj;
        },
        createTable: function () {
            var str = 'create table if not exists `'+this.className+'` (';
            for(var i in this.default_dbparams)
                str += i+' '+this.default_dbparams[i]+' not null,';
            for(var i in this.dbparams)
                str += i+' '+this.dbparams[i]+',';
            str += 'primary key(id));';
            query(str);
        },
        list: function (where,req,cb) {
            var self = this;
            if(isNode) {
                var items = new Array();
                //var instance = new exports[this.className]();
                var wherestr = ' where ';
                this.createTable();
                if(where)
                for(var col in where)
                    if(typeof where[col] === 'string')
                        wherestr += col+"='"+where[col]+"',";
                    else if(where[col] === null)
                        wherestr += col+" is null,";
                    else
                        wherestr += col+"="+where[col]+",";
                if(wherestr.length === 7) wherestr = ',';
                //console.log('select * from `'+this.className+'`'+wherestr.substr(0,wherestr.length-1));
                query('select * from `'+this.className+'`'+wherestr.substr(0,wherestr.length-1),{}, function (err,res) {
                    if(err) {
                        console.log(err);
                        return;
                    }
                    var hurdle = new Hurdle(function () {
                        cb(items);
                    });
                    for(var i = 0;i < res.length;i++) (function (i) {
                        self.loadObj(res[i]);
                        var obj = self.getDBParams();
                        if(typeof self.join === 'function') self.join(req,obj,hurdle);
                        items.push(obj);
                    })(i);
                    hurdle.complete();
                });
            } else {
                services.get({
                    path:'/model/list',
                    data: {model:this.className,where:where}
                }).done(function (res) {
                    res = JSON.parse(res);
                    //self.loadObj(res.model);
                    if(typeof req === 'function')req(res.list);
                    if(typeof where === 'function')where(res.list);
                });
            }
        },
        auth: function (req,cb) {
            cb(true);
        },
        get: function (req,cb) {
            var self = this;
            if(isNode) {
                /*ery('select * from '+self.className+';', {}, function (err,res) {
                    console.log(res,self.getDBParams());
                });*/
                var obj;
                this.createTable();
                query('select * from `'+self.className+'` where id = :id;',self.getDBParams(),function (err,res) {
                    if(err || res.length < 1) {
                        console.log(err);
                        if(cb)cb(false);
                    } else {
                        self.loadObj(res[0]);
                        obj = self.getDBParams();
                        var hurdle = new Hurdle(function () {
                            if (cb) cb(obj);
                        });
                        if(typeof self.join === 'function') self.join(req,obj,hurdle);
                        self.auth(req,function (success) {
                            if (!success) {
                                console.log('access denined');
                                obj = false;
                            }
                            hurdle.complete(obj);
                        });
                    }
                });
            } else {
                services.get({
                    path:'/model',
                    data: {model:this.className,id:this.id}
                }).done(function (res) {
                    res = JSON.parse(res);
                    self.loadObj(res.model);
                    if(cb)cb(self);
                });
            }
        },
        save: function (cb) {
            var self = this;
            if (isNode) {
                var str = '';
                var str2 = '';
                var str3 = '';
                this.createTable();
                for(var i in this.default_dbparams) {
                    if(typeof this[i] !== 'function') {
                        str += ':'+i+',';
                        str2 += i+',';
                        str3 += i+'='+':'+i+',';
                    }
                }
                for(var i in this.dbparams) {
                    if(typeof this[i] !== 'function') {
                        str += ':'+i+',';
                        str2 += i+',';
                        str3 += i+'='+':'+i+',';
                    }
                }
                //console.log('put',this.getDBParams());
                query('insert into `'+self.className+'` ('+str2.substr(0,str2.length-1)+') values ('+str.substr(0,str.length-1)+')',self.getDBParams(), function (err, res) {
                    if(err) {
                        if (err.code === 'ER_DUP_ENTRY') {
                            query('update `'+self.className+'` set '+str3.substr(0,str3.length-1)+' where id=:id;',self.getDBParams(), function (err, res) {
                                if(err && cb) {
                                    console.log(err);
                                    cb(false);
                                } else if(cb)cb(true);
                            });
                        }
                        else {
                            //console.log(err);
                            if(cb)cb(false);
                        }
                    } else if(cb)cb(true);
                });
            } else {
                services.put({
                    path:'/model',
                    data: {model:self.className,data:self.getDBParams()}
                }).done(function (data) {
                    if(cb)cb(JSON.parse(data));
                });
            }
        },
        delete: function (req,cb) {
            var self = this;
            if(isNode) {
                console.log('delete',this.getDBParams());
                this.createTable();
                self.auth(req,function (success) {
                    if (success) {
                        query('delete from `'+self.className+'` where id = :id;',self.getDBParams(),function (err,res) {
                            if(err || res.length < 1) {
                                console.log(err);
                                if(cb)cb(false);
                            } else {
                                if(cb)cb(true);
                            }
                        });
                    } else if(cb)cb(false);
                });
                
            } else {
                services.delete({
                    path:'/model',
                    data: {model:this.className,id:this.id}
                }).done(function () {
                    if(cb)cb(self);
                });
            }
        },
        getHtml: function () {
            
        }
    });

    return {
        extend: function (obj) {
            return SQLObject.prototype.extend(obj);
        },
        configure: configure,
        config: configure,
        Hurdle:Hurdle,
        rand:rand,
        safeTimeout:safeTimeout,
        conn:conn
    };
})();
if(isNode)
    for(var i in JSNodeObject)
        exports[i] = JSNodeObject[i];