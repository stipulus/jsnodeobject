if(typeof document === 'undefined') {
    var oopi = require('../oopi/oopi');
}
//angular directive like behavior
var Component = oopi.class({
    html:null,
    url:null,
    tagName:null,
    className:null,
    wrap: false,
    $elem:null,
    useThread:false,
    hurdle:null,
    template: {},
    construct: function (data,parentHurdle) {
        var component = this;
        if(parentHurdle) parentHurdle.set();
        this.hurdle = {
            i:0,
            set: function () {this.i++;},
            ready: function () {
                this.i--;
                if(this.i <= 0) {
                    component.applyTemplate();
                    if(parentHurdle) parentHurdle.ready();
                }
            }
        };
        if(typeof this.init === 'function') this.init(this.hurdle,data);
        if(typeof this.tagName === 'string') this.$elem = $(this.tagName);
        else this.$elem = $('<div></div>');
        if(this.html) {
            this.hurdle.set();
            this.hurdle.ready();
        } else {
            this.hurdle.set();
            $.get(this.url).then(function (html) {
                component.html = html;
                component.hurdle.ready();
            });
        }
    },
    applyTemplate: function () {
        //async option
        try {
            Mustache.parse(this.html);
            var str = Mustache.render(this.html,this.template);
            if(this.wrap) str = '<div class="'+((this.className)?this.className:"")+'">'+str+'</div>';
            var $elem = $(str);
            //console.log('rendered html',this.html,this.template,str,$elem);
            if(this.$elem.replaceWith)
                this.$elem.replaceWith($elem);
            this.$elem = $elem;
            if(typeof this.ready === 'function') this.ready();
            return true;
        } catch (e) {
            console.error('Could not render template with data.',e);
            return false;
        }
    },
    init: function () {

    },
    add: function (component) {
        this.$elem.parent().append(component.$elem);
    }
});
if(typeof exports !== 'undefined')
    exports.Component = Component;