/*
* http://code.tutsplus.com/tutorials/how-to-create-a-resumable-video-uploader-in-nodejs--net-25445
*/
//https://www.npmjs.com/package/dropbox-node
/*
 * TODO:
 *  -hash password before sending to server on signup and login
 *  -cache pages with hide/show
 *  -create sign up component and add to nav pages
 *  -add image to page title in tab
 *  -set up access control for orders
 *  -upload file cancel
 *  -upload multiple files
 *  -scan for file types: https://en.wikipedia.org/wiki/List_of_file_formats#Video
 */
if(typeof document === 'undefined') {
    var oopi = require('./libs/oopi/oopi');
    var Component = require('./libs/component/component').Component;
    var ThreadFarm = {seed:{prototype:{extend:function () {}}}};
    var $ = function (str) {
        return str;
    }
    $.get = function () {
        return {
            then: function () {

            }
        };
    };
}
var AsyncRender = ThreadFarm.seed.prototype.extend({
    $template:null,
    $target:null,
    needsWrapper:false,
    include:['js/libs/mustache.js/mustache.min.js','js/libs/ThreadFarm/ThreadFarm.js','js/Library.js'],
    apply: function ($target,$template,model) {
        if(typeof model === 'undefined') {
            model = $template;
            $template = $target;
            this.$target = null;
        } else this.$target = $target;
        this.$template = $template;
        this.setData({template:this.$template.html(),model:model});
        this.setBlob(this.buildFunStr());
        this.start();
    },
    run: function (data) {
        if(typeof data.model === 'string') {
            data.model = self[data.model];
        }
        function render() {
            Mustache.parse(data.template);
            data.rendered = Mustache.render(data.template,data.model);
        }
        if(typeof data.template === 'string' && data.template.length < 100) {
            xmlhttp = new XMLHttpRequest();
            xmlhttp.onreadystatechange = function () {
                if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {
                    render();
                }
            };
            xmlhttp.open("GET", data.template, true);
            xmlhttp.send();
        } else render();
    },
    callback: function (res) {
        //console.log(JSON.stringify(res));
        if(this.$target === null) {
            this.$template.before($('<div>'+res.rendered+'</div>'));
            this.needsWrapper = false;
        } else {
            this.$target.html(res.rendered);
        }
    }
});
var AccountDetails = Component.prototype.extend({
    url:'pages/accountDetails.html',
    construct: function () {
        this.template = user.getDBParams();
    },
    init: function () {

    },
    ready: function () {

    },
    submit: function () {

    }
})

var CreateAccount = Component.prototype.extend({
    url:'pages/createAccount.html',
    init: function () {
        //before
    },
    ready: function () {
        //after
        this.$elem.find('.username').val(user.username);
        var $password = this.$elem.find('.password');
        $password.val(user.password);
        this.$elem.find('.password-check').addClass('error');
        this.$elem.find('.password-check').keyup(function () {
            if($(this).val() === $password.val())
                $(this).addClass('error');
            else $(this).removeClass('error');
        });
        this.$elem.find('button').click(this.submit);
        this.$error = this.$elem.find('.error');
        //password re-type checked real time to other password
    },
    submit: function () {
        user.username = $(this).parents('ul').find('.username').val();
        user.password = $(this).parents('ul').find('.password').val();
        user.email = $(this).parents('ul').find('.email').val();
        $('.login').children('input:first').val(user.username);
        $('.login').children('input:eq(1)').val(user.password);
        $('.login').children('button').show();
        user.save(function (data) {
            if(data.ack === 'success') {
                login.login(false);
                nav.to('landing');
            }
        });
    }
});
var Landing = Component.prototype.extend({
    url:'pages/landing.html',
    className:'landing',
    init: function () {

    },
    ready: function () {
        $('nav button').click(nav.to);
    }
});

var ProjectUI = Component.prototype.extend({
    html:'<li>{{order.title}}<span>00:00</span><span>$00.00</span></li>',
    className:'project',
    hurdle:null,
    construct: function (data) {
        this.template = data;
        this.hurdle.ready();
    },
    init: function (hurdle) {
        this.hurdle = hurdle;
        hurdle.set();
    },
    ready: function () {
        var self = this;
        this.$elem.click(function () {
            nav.to('projectDetail',self.template);
        });
    }
});

var ProjectDetail = Component.prototype.extend({
    html:'<h1>{{order.title}}</h1>\
        <p>theme: {{order.theme}}</p>\
        <p>length: 00:00</p>\
        <p>Revenue: $00.00</p>\
        <p><input type="file" /></p>\
        <p><!--a href=\'/download?{"id":"{{id}}"}\'>Download Project</a--><button>Download Project</button></p>\
        <p><button>Accept Contract</button><button>Pass</button></p>\
    ',
    className:'project-detail',
    hurdle:null,
    construct: function (data) {
        this.template = data;
        this.hurdle.ready();
    },
    init: function (hurdle) {
        this.hurdle = hurdle;
        hurdle.set();
    },
    ready: function () {
        var self = this;
        var $upload = this.$elem.children('input[type=file]');
        var $download = this.$elem.children('button:eq(0)');
        var $accept = this.$elem.children('button:eq(1)');
        var $pass = this.$elem.children('button:eq(2)');
        var project = null;
        if(this.template.editorid === user.id)
            $accept.addClass('selected');
        else {
            $download.hide();
            $upload.hide();
        }
        $accept.click(function () {
            self.template.editorid = user.id;
            project = project || new Project(self.template);
            project.save();
            $accept.addClass('selected');
            $download.show();
            $upload.show();
        });
        $pass.click(function () {
            project = project || new Project(self.template);
            if(project.editorid === user.id) {
                if(confirm('Are you sure you want to release this contract?')) {
                    project.editorid = null;
                    self.template.editorid = null;
                    project.save(function () {
                        nav.to('projects');
                    });
                }
            } else {
                nav.to('projects');
            }
        });
        $download.click(function () {
            var text = $download.html();
            var hash = safeTimeout.set(function () {
                $download.html(text);
            }, 30000);
            $download.text('loading...');
            $.get('/download?{"id":"'+self.template.id+'"}', function (data) {
                data = JSON.parse(data);
                if(data.ack && data.ack === 'success') {
                    location.href = data.filename;
                }
                safeTimeout.remove(hash);
                $download.html(text);
            });
        });
    }
});

var MyProjects = Component.prototype.extend({
    html:'<ul></ul>',
    className:'projects',
    projects:null,
    init: function (hurdle) {
        var self = this;
        hurdle.set();
        this.projects = new Array();
        Project.prototype.list({editorid:user.id},function (list) {
            if(list) {
                for(var i = 0;i < list.length;i++) {
                    self.projects[i] = new ProjectUI(list[i]);
                }
                hurdle.ready();
            }
        });
    },
    ready: function () {
        var self = this;
        for(var i = 0;i < this.projects.length;i++) {
            this.$elem.append(this.projects[i].$elem);
        }
    }
});

var Projects = Component.prototype.extend({
    html:'<ul></ul>',
    className:'projects',
    projects:null,
    init: function (hurdle) {
        var self = this;
        hurdle.set();
        this.projects = new Array();
        Project.prototype.list({editorid:null},function (list) {
            if(list) {
                for(var i = 0;i < list.length;i++) {
                    self.projects[i] = new ProjectUI(list[i]);
                }
                hurdle.ready();
            }
        });
    },
    ready: function () {
        var self = this;
        for(var i = 0;i < this.projects.length;i++) {
            this.$elem.append(this.projects[i].$elem);
        }
    }
});

var header = {
    init: function () {

    }
};

var footer = {
    init: function () {
        
    }
};

var body = {
    $elem:null,
    beforeSets:new Array(),
    currentComponent: {className:"landing"},
    init: function () {
        this.$elem = $('section.body');
    },
    set: function (component) {
        var self = this;
        this.show();
        this.beforeSet();
        if(component) {
            if(typeof component.className === 'string') {
                if(typeof this.currentComponent.className === 'string')
                    this.$elem.removeClass(this.currentComponent.className);
                this.$elem.addClass(component.className);
            }
            this.$elem.html(component.$elem);
            this.currentComponent = component;
        }
    },
    beforeSet: function () {
        while(this.beforeSets.length > 0)
            this.beforeSets.pop()();
    },
    addBeforeSet: function (fun) {
        if(typeof fun == 'function')
            this.beforeSets.push(fun);
    },
    hide: function () {
        this.$elem.hide();
    },
    show: function () {
        this.$elem.show();
    }
};

var ui = {
    init:function () {
        //var user = new User();
        //user.username = 'tyler';
        //user.password = 'pass';
        //user.save();
        //User.prototype.list();
        //Order.prototype.list();
        account.init();
        login.init();
        header.init();
        footer.init();
        nav.init();
        body.init();
        $('body').click(this.clickAway);
        $('.logo').click(function () {
            nav.to('landing');
        });
        if(location.hash === '#projectDetail') location.hash = '#projects';
        if(!location.hash || location.hash.length < 2 || !nav.to(location.hash.substr(1)))
            nav.to('landing');
    },
    clickAwayEvent:[],
    addClickAwayEvent: function (fun) {
    	if(typeof fun === 'function')
	    	setTimeout(function() {
				ui.clickAwayEvent.push(fun);
	    	}, 10);
    },
    clickAway: function () {
    	while(ui.clickAwayEvent.length > 0)
    		ui.clickAwayEvent.pop()();
    },
    notifying: false,
    notify: function (str) {
        var $elem = $('.notification');
        $elem.text(str);
        if($elem.is(':hidden')) {
            $elem.fadeIn(function () {
                setTimeout(function () {
                    $elem.fadeOut();
                },2000);
            });
        }
    }
};

var nav = {
    $elem: null,
    init: function () {
        this.$elem = $('nav');
        this.$elem.find('a').click(this.to);
    },
    reload: function () {
        this.to(location.hash.substr(1));
    },
    to: function (str,data) {
        var $elem;
        if(typeof str !== 'string') {
            str = $(this).text();
            $elem = $(this);
        }
        var obj = null;
        switch (str) {
            case'landing':
            case'projects':
            case'PROJECTS':
                obj = new Projects();
                str = 'PROJECTS';
                nav.select(str,obj,$elem);
                break;
            case 'MY PROJECTS':
                if(account.hurdle.i > 0)
                    account.hurdle.callback = function () {
                        obj = new MyProjects();
                        nav.select(str,obj,$elem);
                    };
                else {
                    obj = new MyProjects();
                    nav.select(str,obj,$elem);
                }
                break;
            case'projectDetail':
                obj = new ProjectDetail(data);
                nav.select(str,obj,$elem);
                break;
            default:
            console.error('Page not found: '+str);
            return false;
            break;
        }
        return true;
    },
    select: function (str,obj,$elem) {
        if(!$elem) {
            this.$elem.find('a').each(function () {
                if($(this).text() === str)
                    $elem = $(this);
            })
        }
        if($elem) {
            $elem.parent('li').siblings().children('a.selected').removeClass('selected');
            $elem.addClass('selected');
        }
        if(location.hash) history.pushState(null, null, location.hash);
        location.hash = '#'+str;
        body.set(obj);
    }
};

var login = {
    init: function () {
        $('.login > div > button:first').click(this.login);
        $('.login > div > button:eq(1)').click(this.signup);
        $('.login > div > input').keydown(function (e) {
            if(e.which === 13) {
                $(this).parent('div').children('button:first').trigger('click');
            }
        });
    },
    onLoginStack: [],
    callOnLogin: function (fun) {
        if(typeof fun === 'function')
            this.onLoginStack.push(fun);
    },
    onLogin: function () {
        while(this.onLoginStack.length > 0)
            this.onLoginStack.pop()();
    },
    login: function (useUser) {
        if(useUser) {
            user.username = $(this).parent('div').children('input:first').val();
            user.password = $(this).parent('div').children('input:eq(1)').val();
        }
        var $error = $(this).parent('div').children('div');
        user.login(function (success) {
            if(success) {
                account.setCurrentUser(user.username);
                $error.hide();
                login.onLogin();
                nav.reload();
            }
            else {
                $error.text('Invalid username or password');
                $error.slideDown('fast');
            }
        });
    },
    signup: function () {
        user.username = $(this).parent('div').children('input:first').val();
        user.password = $(this).parent('div').children('input:eq(1)').val();
        $(this).hide();
        nav.to('createAccount');
        var self = this;
        body.addBeforeSet(function () {
            $(self).show();
        })
    }
};

var account = {
    hurdle:null,
	init: function () {
        this.hurdle = new Hurdle();
        user = new User({});
        user.getCurrent(function () {
            account.setCurrentUser(user.username);
            account.hurdle.complete();
        });
		$('.account > button').click(function () {
			var $ul = $(this).next('ul');
			if($ul.is(':hidden')) {
				ui.addClickAwayEvent(function () {
					$ul.fadeOut();
				});
			}
            $ul.fadeToggle();
		});
		$('.account ul li').click(function () {
			$(this).siblings('li').css('color','transparent');
			$(this).fadeOut(function () {
				$(this).parent('ul').hide();
				$(this).parent('ul').children('li').css('color','white');
				$(this).parent('ul').children('li').show();
			});
		});
        $('.account .logout').click(this.logout);
        $('.account .details').click(function () {
            nav.to('accountDetails');
        });
	},
    logout: function () {
        user.logout(function (success) {
            if(success)
                account.setCurrentUser();
        });
    },
    setCurrentUser: function (user) {
        console.log(user);
        if(user) {
            $('.login').hide();
            $('.account').show();
            $('.current-user').text(user);
        } else {
            $('.login').show();
            $('.account').hide();
            $('.current-user').text('');
        }
    }

};



if(typeof document === 'undefined') {
}